# terraform-demo

This Terraform demo project deploys a Google Cloud Scheduler Job which posts a message to a Google Cloud Pub/Sub topic once per minute.
You can use these messages to trigger other jobs, for example a Google Cloud Function.

## Setup

This Terraform project uses Gitlab as state backend.
To set up, replace the Gitlab project ID in `setup.tf` and then run

```
terraform init -backend-config="password=<gitlab-token>"
```

where `<gitlab-token>` is a Gitlab access token with at least `api` permissions on the project.

To authenticate using your Google Cloud service account, follow these instructions: https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#authentication-configuration

You can then deploy the infrastructure using

```
terraform apply
```

## What's next?

- Deploy something that consumes the messages.
- Set up CI/CD by creating Gitlab CI jobs that automatically apply Terraform.