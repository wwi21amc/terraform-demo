terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/58047164/terraform/state/default"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
    retry_wait_max = 15
    username       = "terraform"

    # you need to provide the password like this
    # terraform init -backend-config="password=<gitlab-token>"
  }
}

provider "google" {
  project = "wwi21amc-demo"
  region  = "europe-west1"
}