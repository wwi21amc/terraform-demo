

# The Pub/Sub topic we want the scheduler to publish messages in
resource "google_pubsub_topic" "topic" {
  name = "scheduled"
}

# The Scheduler job that will publish a message to the topic every hour
resource "google_cloud_scheduler_job" "job" {
  name             = "scheduler-job"
  schedule         = "* * * * *" # Every minute
  time_zone        = "Etc/UTC"
  pubsub_target {
    topic_name = google_pubsub_topic.topic.id
    data       = base64encode(jsonencode({}))
  }
}
